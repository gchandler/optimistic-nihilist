// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "ONegggameGameMode.h"
#include "ONegggamePawn.h"
#include "ONegggameHud.h"

AONegggameGameMode::AONegggameGameMode()
{
	DefaultPawnClass = AONegggamePawn::StaticClass();
	HUDClass = AONegggameHud::StaticClass();
}
