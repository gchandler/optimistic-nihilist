// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VehicleWheel.h"
#include "ONegggameWheelRear.generated.h"

UCLASS()
class UONegggameWheelRear : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UONegggameWheelRear();
};



