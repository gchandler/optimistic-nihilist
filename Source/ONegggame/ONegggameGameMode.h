// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ONegggameGameMode.generated.h"

UCLASS(MinimalAPI)
class AONegggameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AONegggameGameMode();
};



